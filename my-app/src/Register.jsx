import React, {useState} from "react";

export const Register = (props) => {

    const [email, setEmail] = useState('');
    const [pass, setPass] = useState('');
    const [name, setName] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('Reisgter sucessfull with : ' + email)
    }

    return (
        <div className="auth-form-container">
            <h1>Register Page</h1>

            <form className="register-form" onSubmit={handleSubmit}>

                <label htmlFor="name">FullName</label>
                <input value={name}
                       name="name"
                       id="name"
                       onChange={(e) => setName(e.target.value)}
                       placeholder="FULL NAME"/>

                <label htmlFor="email">Email</label>
                <input value={email}
                       type="email" onChange={(e) => setEmail(e.target.value)}/>

                <label htmlFor="password">Password</label>
                <input value={pass}
                       type="password" onChange={(e) => setPass(e.target.value)}/>

                <button type="submit">Register</button>
            </form>

            <button onClick={() => props.onFormSwitch('login')}>Already have an account ? Login Here</button>
        </div>
    )
}


