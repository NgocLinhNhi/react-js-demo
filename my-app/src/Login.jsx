import React, {useState} from "react";

export const Login = (props) => {
    const [email, setEmail] = useState(''); // set giá trị cho hàm setEMail
    const [pass, setPass] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(email + ' - password :' + pass);
        console.log("Login SuccessFul")
    }

    return (
        <div className="auth-form-container">
            <h1>Login Page</h1>

            <form className="login-form" onSubmit={handleSubmit}>
                <label htmlFor="email">email</label>
                <input value={email}
                       type="email"
                       onChange={(e) => setEmail(e.target.value)}/>

                <label htmlFor="password">Password</label>
                <input value={pass}
                       type="password"
                       onChange={(e) => setPass(e.target.value)}/>

                <button type="submit">Login</button>
            </form>

            <button className="link-btn" onClick={() => props.onFormSwitch('register')}>
                Dont have a new account? Register here
            </button>
        </div>
    )
}
